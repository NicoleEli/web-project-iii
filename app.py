"""================================================================================================
Institute....: Universidad Técnica Nacional
Headquarters.: Pacífico
Career.......: Tecnologías de la Información
Period.......: II-2021
Document.....: clsConexion.py
Student......: Nicole ELizondo Soto, Gerald Bolaños Delgado, Orlanffer Rodríguez Velarde
================================================================================================"""

from server import Websocket, WebsocketServer
from clsConexion import clsConexion

conex = clsConexion()


class wsEvents(Websocket):
    def onMessage(self, varios):
        self.broadcast(varios)

    def onConnect(self):
        print(len(self.connections))

    def onDisconnect(self):
        print('Close')


server = WebsocketServer(ws_cls=wsEvents)
server.run()
