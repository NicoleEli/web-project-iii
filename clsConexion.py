"""================================================================================================
Institute....: Universidad Técnica Nacional
Headquarters.: Pacífico
Career.......: Tecnologías de la Información
Period.......: II-2021
Document.....: clsConexion.py
Student......: Nicole ELizondo Soto, Gerald Bolaños Delgado, Orlanffer Rodríguez Velarde
================================================================================================"""
from os.path import splitext
import locale
import os
import pymysql


class clsConexion():
    # Declara las variables para la conexion
    _servidor = 'localhost'  # Recuerde cambiar la dirección y contraseña
    _basedatos = 'Chats'
    _usuario = 'root'
    _contra = ''

    def __init__(self):
        pass

    @property
    def _conectar(self):
        try:
            _conex = pymysql.connect(host='localhost',
                                     user='root',
                                     passwd='',
                                     db='Chats')

        except Exception as err:
            print(err)
        return _conex

    # Agregar ---------------------------------------------------------------------------------------------------
    def agregar_Usuario(self, est, nom, id):
        estado = False
        AuxSql = "insert into users(estado, nombre, id) values('{0}', '{1}', '{2}');". \
            format(est, nom, id)
        try:
            _conex = self._conectar
            with _conex.cursor() as cursor:
                cursor.execute(AuxSql)
            _conex.commit()
            _conex.close()
            estado = True
        except Exception as err:
            print(err)
        return estado

    def agregar_mensaje(self, de, para, sms):
        estado = False
        AuxSql = "insert into mensajes(remitante, destinatario, messagge) values('{0}', '{1}', '{2}');". \
            format(de, para, sms)
        try:
            _conex = self._conectar
            with _conex.cursor() as cursor:
                cursor.execute(AuxSql)
            _conex.commit()
            _conex.close()
            estado = True
        except Exception as err:
            print(err)
        return estado

    # Update -------------------------------------------------------------------------------------------------------

    def editar_usuario(self, id, est):
        AuxSql = "update users set estado ='{1}' where id = '{0}';" \
            .format(id, est)
        estado = False
        try:
            _conex = self._conectar
            with _conex.cursor() as cursor:
                cursor.execute(AuxSql)
            _conex.commit()
            _conex.close()
            estado = True
        except Exception as err:
            print(err)
        return estado

    # consultar -------------------------------------------------------------------------------------------------

    def consultar_mensajes(self, idE, idR):
        data = ''
        salida = ""
        try:
            _conex = self._conectar
            with _conex.cursor() as cursor:
                cursor.execute("select messagge from mensajes where remitante='{0}' and destinatario='{1}' or remitante='{1}' and destinatario='{0}';".
                               format(idE, idR))
                _conex.commit()
                data = cursor.fetchall()

            _conex.close()
        except Exception as err:
            print(err)

        for tupla in data:
            salida += tupla[0]

        return salida

    def consultar_us(self, id):
        data = ''
        try:
            _conex = self._conectar
            with _conex.cursor() as cursor:
                cursor.execute("select * from users where id = '{0}';".
                               format(id))
                _conex.commit()
                data = cursor.fetchall()

            _conex.close()
        except Exception as err:
            print(err)

        salida = False
        for tupla in data:
            if tupla[0] == "":
                salida = False
            else:
                salida = True

        return salida

    def consultar_Usuario(self, para):
        data = ''
        salida = ""
        try:
            _conex = self._conectar
            with _conex.cursor() as cursor:
                cursor.execute("select estado from users where  id = '{0}';".
                               format(para))
                _conex.commit()
                data = cursor.fetchall()

            _conex.close()
        except Exception as err:
            print(err)

        for tupla in data:
            salida = tupla[0]

        return salida

    def consultar_id(self, para):
        data = ''
        salida = ""
        try:
            _conex = self._conectar
            with _conex.cursor() as cursor:
                cursor.execute("select id from users where nombre = '{0}';".
                               format(para))
                _conex.commit()
                data = cursor.fetchall()

            _conex.close()
        except Exception as err:
            print(err)

        for tupla in data:
            salida = tupla[0]

        return salida
